"""Stac extension generic metadata module."""

from .core import create_extension_cls, BaseExtensionModel  # noqa

__version__ = "0.1.3-dev1"
